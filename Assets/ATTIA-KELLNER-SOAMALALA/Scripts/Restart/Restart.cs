﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Restart : MonoBehaviour
{
    public VictoryPlatform VictoryPlatform;
    public Slider sliderFaim;
    public Slider sliderSante;

    private bool reset = false;

    // Start is called before the first frame update
    void Start()
    {
        sliderFaim = GameObject.FindGameObjectWithTag("eatslider").GetComponent<Slider>();
        sliderSante = GameObject.FindGameObjectWithTag("healthslider").GetComponent<Slider>();
    }

    // Update is called once per frame
    void Update()
    {

        if (reset)
        {
            // set gamers to start position
            VictoryPlatform.SetVictory(false);
            reset = false;
            SceneManager.LoadScene("SCENE_FINAL");
            setAfterOnLoadScene();

        }
    }

    public void SetReset(bool value)
    {
        reset = value;
    }

    IEnumerator setAfterOnLoadScene()
    {
        yield return new WaitForSeconds(Time.deltaTime);
    }
}
