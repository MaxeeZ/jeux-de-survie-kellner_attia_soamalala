﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetDetection : MonoBehaviour
{
    private GameObject target;
    public float maxDistanceDetection = 7.47f;
    public float angleChampDeVision = 38.6f;

    private float lerpTime = 1f; // tps entre le startPos et le endPos
    private float currentLerpTime; // où il en est actuellement

    private Vector3 startPos; // position pour le lerp
    private Vector3 endPos; // position pour le lerp

    private bool isLerping = false;
    private bool combat = false;
    private bool enableDetectObjectInFieldOfView = false;

    // set distance zone de dectection
    private CapsuleCollider zoneDetection;
    
    void Start()
    {
        zoneDetection = GetComponent<CapsuleCollider>();
        zoneDetection.radius = maxDistanceDetection;
    }

    void Update()
    {
        if (target != null)
        {
            if (enableDetectObjectInFieldOfView)
            {
                if(ObjectDetectedInFieldOfView())
                {
                    Debug.Log("L'ennemie confirme qu'il a bien le joueur dans sa ligne de mire !");
                    updateLerpingParams();
                }
                else
                {
                    enableDetectObjectInFieldOfView = true;
                    isLerping = false;
                }
            }
            else
            {
                if (playerHasMoved())
                {
                    updateLerpingParams();
                }
            }
        }

        if (isLerping)
        {
            currentLerpTime += Time.deltaTime;

            if (currentLerpTime > lerpTime)
            {
                currentLerpTime = lerpTime;
            }

            float pourcentage = currentLerpTime / lerpTime;

            Vector3 relativePos = endPos - startPos;

            transform.rotation = Quaternion.LookRotation(relativePos, Vector3.up);
            transform.position = Vector3.Lerp(startPos, endPos, pourcentage);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("L'ennemie detecte une présence dans sa zone !");
            enableDetectObjectInFieldOfView = true;
            target = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("L'ennemie ne détecte plus de présence dans sa zone !");
            target = null;
            isLerping = false;
            enableDetectObjectInFieldOfView = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("L'ennemie est en collision avec le joueur !");
            combat = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("L'ennemie n'est plus en collision avec le joueur !");
            combat = false;
        }
    }

    private void updateLerpingParams()
    {
        endPos = target.transform.position;
        startPos = transform.position;

        currentLerpTime = 0f;
        isLerping = true;
    }

    private bool playerHasMoved()
    {
        if (endPos != target.transform.position)
        {
            return true;
        }

        return false;
    }

    private bool ObjectDetectedInFieldOfView()
    {
        Vector3 origin = transform.position + Vector3.zero;
        Vector3 destination = target.transform.position - origin;
        LayerMask DetectionLayer = ~0;

        // Eviter les détections à travers les murs
        Debug.Log("Mur présent ? : " + Physics.Raycast(origin, destination, out RaycastHit hit, maxDistanceDetection, DetectionLayer));
        Debug.Log(hit.collider.name);
        if (hit.collider.name == "Player")
        {
            var direction = target.transform.position - transform.position;

            // Vérifier que le player est dans le champ de vision
            if (Vector3.Angle(direction, transform.forward) <= angleChampDeVision)
            {
                return true;
            }
        }

        return false;
    }

    public bool GetIsLerping() {
        return isLerping;
    }

    public bool GetCombat() {
        return combat;
    }

}
