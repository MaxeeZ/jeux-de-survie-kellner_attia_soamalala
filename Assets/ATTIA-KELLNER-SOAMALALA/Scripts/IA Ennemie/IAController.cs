﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IAController : MonoBehaviour
{

    public TargetDetection targetDetection;
    public GameObject IaPlayer;
    public float gravity = 20f;
    
    private Vector3 moveDirection = Vector3.zero;

    private CharacterController ia;
    private NavMeshAgent agent;

    public bool iaCanMove = true;

    private Animator iaAnimator = null;

    // Start is called before the first frame update
    void Start()
    {
        ia = GetComponent<CharacterController>();
        agent = GetComponent<NavMeshAgent>();
        iaAnimator = IaPlayer.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        // Ici je voulais utiliser en gros le déplacement aléatoire dans le NavMesh, par contre lorsque l'IA ennemie trouve une cible (targetDetection.GetTarget() != null)
        // il me fallait désactiver ce mouvement aléatoire dans le labyrinthe pour profiter du Lerp sur ma target et des que je perd la target soit quand isLerping = false
        // isLearping == true => detecte une cible dans ma zone de détection + vision sur la cible sans obstacle + cible dans le champ de vision donc
        // isLerping == false => je perds ma target de vue donc je remet en marche mon algo pour le déplacement aléatoire via le navmesh

        if (ia.isGrounded && iaCanMove && targetDetection.GetIsLerping() == false)
        {
            iaCanMove = false;
            //iaAnimator.SetTrigger("Run");
            StartCoroutine(MoveIAWithTimeOut(5.0f, agent));
        }

        if (targetDetection.GetIsLerping() == false)
        {
            if (!iaAnimator.GetBool("WalkNoRun"))
            {
                iaAnimator.SetBool("WalkNoRun", true);
            }
        }
        else
        {
            if (iaAnimator.GetBool("WalkNoRun"))
            {
                iaAnimator.SetBool("WalkNoRun", false);
            }
        }

        moveDirection.y -= gravity * Time.deltaTime;
        ia.Move(moveDirection * Time.deltaTime);
    }

    public Vector3 RandomNavmeshLocation(float distanceMax)
    {
        Vector3 randomDirection = Random.insideUnitSphere * distanceMax;
        randomDirection += transform.position;

        NavMeshHit hit;

        Vector3 finalPosition = Vector3.zero;

        if (NavMesh.SamplePosition(randomDirection, out hit, distanceMax, 1))
        {
            finalPosition = hit.position;
        }

        return finalPosition;
    }

    IEnumerator MoveIAWithTimeOut(float waitTime, NavMeshAgent agent)
    {
        agent.SetDestination(RandomNavmeshLocation(30.0f));
        yield return new WaitForSeconds(waitTime);
        iaCanMove = true;
    }
}
