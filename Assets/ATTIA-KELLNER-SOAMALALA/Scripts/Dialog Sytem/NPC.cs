﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NPC file", menuName = "NPC Files Archive", order = 0)]
public class NPC : ScriptableObject
{
    
    public new string name;

    public string[] dialogue;

    public string [] playerDialogue;

}
