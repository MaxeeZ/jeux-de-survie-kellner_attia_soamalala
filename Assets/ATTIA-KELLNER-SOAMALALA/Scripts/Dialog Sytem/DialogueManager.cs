﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{

    public NPC npc;

    bool isTalking = false;

    float distance;
    float curResponseTracker = 0;

    public GameObject player;
    public GameObject dialogueUI;
    public GameObject interactionUI;

    public Text npcName;
    public Text npcDialogueBox;
    public Text playerResponse;

    private Animator animator = null;

    // Start is called before the first frame update
    void Start()
    {
        dialogueUI.SetActive(false);
        animator = GetComponent<Animator>();
    }


    void Update()
    {
        distance = Vector3.Distance(player.transform.position, this.transform.position);

        if (distance > 4.0f)
        {
            if (isTalking)
            {
                EndDialogue();
            }

            interactionUI.SetActive(false);
        }

        if (distance <= 4.0f)
        {
            interactionUI.SetActive(false);

            if (isTalking == false)
            {
                interactionUI.SetActive(true);
            }

            if (Input.GetAxis("Mouse ScrollWheel") < 0f)
            {
                curResponseTracker++;
                if (curResponseTracker >= npc.playerDialogue.Length - 1)
                {
                    curResponseTracker = npc.playerDialogue.Length - 1;
                }
            }
            else if (Input.GetAxis("Mouse ScrollWheel") > 0f)
            {
                curResponseTracker--;
                if (curResponseTracker < 0)
                {
                    curResponseTracker = 0;
                }
            }

            // trigger dialogue
            if (Input.GetKeyDown(KeyCode.E) && isTalking == false)
            {
                animator.SetTrigger("Hello");
                StartConversation();
            }
            else if (Input.GetKeyDown(KeyCode.E) && isTalking == true)
            {
                EndDialogue();
            }

            if (curResponseTracker == 0 && npc.playerDialogue.Length >= 0)
            {
                playerResponse.text = npc.playerDialogue[0];
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    animator.SetTrigger("Talking");
                    npcDialogueBox.text = npc.dialogue[1];
                }
            }
            else if (curResponseTracker == 1 && npc.playerDialogue.Length >= 1)
            {
                playerResponse.text = npc.playerDialogue[1];
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    animator.SetTrigger("Talking");
                    npcDialogueBox.text = npc.dialogue[2];
                }
            }
            else if (curResponseTracker == 2 && npc.playerDialogue.Length >= 2)
            {
                playerResponse.text = npc.playerDialogue[2];
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    animator.SetTrigger("Talking");
                    npcDialogueBox.text = npc.dialogue[3];
                }
            }
            else if (curResponseTracker == 3 && npc.playerDialogue.Length >= 3)
            {
                playerResponse.text = npc.playerDialogue[3];
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    EndDialogue();
                }
            }
        }
    }

    void StartConversation()
    {
        isTalking = true;
        curResponseTracker = 0;
        dialogueUI.SetActive(true);
        npcName.text = npc.name;
        npcDialogueBox.text = npc.dialogue[0];
    }

    void EndDialogue()
    {
        isTalking = false;
        dialogueUI.SetActive(false);
        animator.SetTrigger("Bye");
    }

}
