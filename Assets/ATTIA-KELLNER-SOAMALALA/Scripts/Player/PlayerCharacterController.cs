﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacterController : MonoBehaviour
{
    public float speed = 15f;
    public float jumpSpeed = 8f;
    public float gravity = 20f;
    public int rotateSpeed = 20;
    
    private Vector3 moveDirection = Vector3.zero;

    CharacterController pcc;
    public GameObject playerModelAnimation;

    private Animator animatePlayerModel = null;

    // Start is called before the first frame update
    void Start()
    {
        pcc = GetComponent<CharacterController>();
        animatePlayerModel = playerModelAnimation.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (pcc.isGrounded)
        {
            moveDirection = new Vector3(0, 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;

            if (Input.GetButtonDown("Jump"))
            {
                moveDirection.y = jumpSpeed;
            }
        }

        moveDirection.y -= gravity * Time.deltaTime;

        transform.Rotate(Vector3.up * Input.GetAxis("Horizontal") * Time.deltaTime * speed * rotateSpeed);

        if(Input.GetAxis("Vertical") != 0f)
        {
            if(!animatePlayerModel.GetBool("Run"))
            {
                animatePlayerModel.SetBool("Run", true);
            }
        }
        else
        {
            if(animatePlayerModel.GetBool("Run"))
            {
                animatePlayerModel.SetBool("Run", false);
            }
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("SPACE");
            if(!animatePlayerModel.GetBool("Jump"))
            {
                animatePlayerModel.SetBool("Run", false);
                animatePlayerModel.SetBool("Jump", true);
                Debug.Log("JUMP: run -> " + animatePlayerModel.GetBool("Run") + " jump -> " + animatePlayerModel.GetBool("Jump"));
            }
        }
        else
        {
            if(animatePlayerModel.GetBool("Jump"))
            {
                animatePlayerModel.SetBool("Jump", false);
                animatePlayerModel.SetBool("Run", true);
            }
        }
        

        pcc.Move(moveDirection * Time.deltaTime);
    }
    
}
