﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCCharacter : MonoBehaviour
{
    public float gravity = 20f;
    
    private Vector3 moveDirection = Vector3.zero;

    CharacterController npc;

    // Start is called before the first frame update
    void Start()
    {
        npc = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (npc.isGrounded)
        {
            moveDirection.y -= gravity * Time.deltaTime;
            npc.Move(moveDirection * Time.deltaTime);
        }

    }
}

