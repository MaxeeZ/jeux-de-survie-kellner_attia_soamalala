﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VictoryPlatform : MonoBehaviour
{

    public GameObject player;
    public GameObject victoryUI;

    private bool victory;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if ( (Vector3.Distance(player.transform.position, transform.position)) < 3.0f )
        {
            Debug.Log("VICTOIRE");
            victory = true;
        }

        if (victory)
        {
            victoryUI.SetActive(true);
        }
        else
        {
            victoryUI.SetActive(false);
        }
        
    }

    public bool GetVictory()
    {
        return victory;
    }

    public void SetVictory(bool value)
    {
        victory = value;
    }
}
