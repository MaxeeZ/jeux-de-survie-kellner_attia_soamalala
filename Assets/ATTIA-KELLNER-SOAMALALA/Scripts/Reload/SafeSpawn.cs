﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SafeSpawn : MonoBehaviour
{

    public GameObject player;

    private Slider sliderFaim;
    private Slider sliderSante;

    // Start is called before the first frame update
    void Start()
    {
        sliderFaim = GameObject.FindGameObjectWithTag("eatslider").GetComponent<Slider>();
        sliderSante = GameObject.FindGameObjectWithTag("healthslider").GetComponent<Slider>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter()
    {
        Debug.Log("SPAWN");
        sliderFaim.value = sliderFaim.maxValue;
        sliderSante.value = sliderSante.maxValue;
    }
}
