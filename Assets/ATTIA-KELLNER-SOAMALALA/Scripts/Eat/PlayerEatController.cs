﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerEatController : MonoBehaviour
{

    private Slider sliderFaim;
    private Slider sliderSante;

    public TargetDetection targetDetection1;
    public TargetDetection targetDetection2;
    public GameObject loserUI;

    public int maxValueHealth;
    public int maxValueEat;

    public static float timeLostLife = 10.0f;
    private float nextActionTime = timeLostLife;

    // Start is called before the first frame update
    void Start()
    {
        InitializeEatAndHealth();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextActionTime )
        {
            nextActionTime += timeLostLife;
            UpdateFaimEtSante();
        }

        if (targetDetection1.GetCombat() || targetDetection2.GetCombat())
        {
            ReduireSanteOuFaim(1, sliderSante);
        }

        if (sliderSante.value <= 0)
        {
            loserUI.SetActive(true);
            GetComponent<CharacterController>().enabled = false;
        }
        else
        {
            loserUI.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.tag)
        {
            case "EAT 15":
                other.gameObject.SetActive(false);
                AugmenterFaim(15, sliderFaim);
                break;

            case "EAT 10":
                other.gameObject.SetActive(false);
                AugmenterFaim(10, sliderFaim);
                break;

            case "EAT 5":
                other.gameObject.SetActive(false);
                AugmenterFaim(5, sliderFaim);
                break;

            default:
                break;
        }
    }

    private void InitializeEatAndHealth()
    {
        sliderFaim = GameObject.FindGameObjectWithTag("eatslider").GetComponent<Slider>();
        sliderSante = GameObject.FindGameObjectWithTag("healthslider").GetComponent<Slider>();

        sliderFaim.maxValue = maxValueEat;
        sliderFaim.value = maxValueEat;

        Debug.Log("Etat Faim: " + sliderFaim.value);

        sliderSante.maxValue = maxValueHealth;
        sliderSante.value = maxValueHealth;

        Debug.Log("Etat Sante: " + sliderFaim.value);
    }

    private void AugmenterSante(int value)
    {
        if ((sliderSante.value + value ) >= 100) {
            sliderSante.value = 100;
        }
        else
        {
            sliderSante.value += value;
        }
    }

    private void AugmenterFaim(int value, Slider slider)
    {
        if ((sliderFaim.value + value ) >= 100) {
            sliderFaim.value = 100;
            AugmenterSante(20);
        }
        else
        {
            sliderFaim.value += value;
        }
    }

    private void ReduireSanteOuFaim(int value, Slider slider)
    {
        if ((slider.value - value ) <= 0) {
            slider.value = 0;
        }
        else
        {
            slider.value -= value;
        }
    }

    private void UpdateFaimEtSante()
    {
        ReduireSanteOuFaim(10, sliderFaim);

        if (sliderFaim.value == 0)
        {
            ReduireSanteOuFaim(10, sliderSante);
        }
    }

}
