﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EatObjectController : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        foreach(Transform child in gameObject.transform)
        {
            if (!child.gameObject.activeSelf) {
                StartCoroutine(ActiveChildWithTimeOut(50.0f, child));
            }
        }
    }

    IEnumerator ActiveChildWithTimeOut(float waitTime, Transform child)
    {
        yield return new WaitForSeconds(waitTime);
        child.gameObject.SetActive(true);
    }
}
